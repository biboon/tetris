#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>

#include <GL/glut.h>


#define GRID_COLS (10)
#define GRID_ROWS (25)


typedef struct tetro_s
{
    uint16_t shape;
    uint32_t color;
    int col, row;
} tetro_t;

typedef uint32_t grid_t[GRID_ROWS][GRID_COLS];
typedef struct timeval date_t;


#define COLOR_RGBA(r, g, b, a) ((r) | ((g) << 8) | ((b) << 16) | ((a) << 24))
#define COLOR_R(color) ( (color)        & 0xff)
#define COLOR_G(color) (((color) >>  8) & 0xff)
#define COLOR_B(color) (((color) >> 16) & 0xff)
#define COLOR_A(color) (((color) >> 24) & 0xff)


static tetro_t g_tetro;
static grid_t  g_grid, g_grid_committed;
static date_t  g_date_fall;


void tetro_transform(tetro_t *t, int col, int row, int rot)
{
#define mask ((1 << 0) | (1 << 4) | (1 << 8) | (1 << 12))

    uint32_t res = t->shape;

    switch ((rot + 4) % 4)
    {
        case 3: res = ((res << 1) & ~mask) | ((res >> 3) & mask);
        case 2: res = ((res << 1) & ~mask) | ((res >> 3) & mask);
        case 1: res = ((res << 1) & ~mask) | ((res >> 3) & mask);

        case 0: break;
        default: assert( !"Rotations can only be 0-3");
    }

    t->shape = res;
    t->col  += col;
    t->row  += row;

#undef mask
}

void tetro_init(tetro_t *t)
{
    t->col = GRID_COLS / 2;
    t->row = GRID_ROWS - 4;

    switch (rand() % 7)
    {
        case 0: /* Stick */
            t->color = COLOR_RGBA(0, 255, 255, 255);
            t->shape = ((1 << 0xB) | (1 << 0xC) | (1 << 0xD) | (1 << 0x5));
            break;

        case 1: /* Square */
            t->color = COLOR_RGBA(255, 255, 0, 255);
            t->shape = ((1 << 0xC) | (1 << 0xF) | (1 << 0xD) | (1 << 0xE));
            break;

        case 2: /* S shaped */
            t->color = COLOR_RGBA(0, 255, 0, 255);
            t->shape = ((1 << 0xC) | (1 << 0x4) | (1 << 0xB) | (1 << 0x7));
            break;

        case 3: /* Z shaped */
            t->color = COLOR_RGBA(255, 0, 0, 255);
            t->shape = ((1 << 0xC) | (1 << 0x0) | (1 << 0xB) | (1 << 0xF));
            break;

        case 4: /* L shaped */
            t->color = COLOR_RGBA(0, 0, 255, 255);
            t->shape = ((1 << 0xC) | (1 << 0xF) | (1 << 0xA) | (1 << 0xD));
            break;

        case 5: /* J shaped */
            t->color = COLOR_RGBA(255, 128, 0, 255);
            t->shape = ((1 << 0xD) | (1 << 0xE) | (1 << 0x6) | (1 << 0xC));
            break;

        case 6: /* T shaped */
            t->color = COLOR_RGBA(128, 0, 255, 255);
            t->shape = ((1 << 0xC) | (1 << 0x4) | (1 << 0xB) | (1 << 0xF));
            break;

        default: assert( !"invalid tetronimo type" );
    }

    tetro_transform(t, 0, 0, rand());
}


void grid_cpy(grid_t *dst, const grid_t *src)
{
    memcpy(dst, src, sizeof(grid_t));
}

enum
{
    GI_OK,
    GI_COLLISION,
    GI_HIT_TOP,
    GI_HIT_BOT,
    GI_HIT_LEFT,
    GI_HIT_RIGHT,
};

int grid_insert_tetro(grid_t *g, const tetro_t *t)
{
    int map[4][4] =
    {
        { 1 << 0x0, 1 << 0xB, 1 << 0x7, 1 << 0x3 },
        { 1 << 0x4, 1 << 0xC, 1 << 0xF, 1 << 0xA },
        { 1 << 0x8, 1 << 0xD, 1 << 0xE, 1 << 0x6 },
        { 1 << 0x1, 1 << 0x5, 1 << 0x9, 1 << 0x2 },
    };

    for (int srow = 0; srow < 4; ++srow)
    {
        for (int scol = 0; scol < 4; ++scol)
        {
            int gcol, grow;

            if ( !(t->shape & map[srow][scol]) )
                continue;

            gcol = scol + t->col;
            grow = srow + t->row;

            if ( grow < 0 )              return GI_HIT_BOT;
            if ( gcol < 0 )              return GI_HIT_LEFT;
            if ( GRID_ROWS <= grow )     return GI_HIT_TOP;
            if ( GRID_COLS <= gcol )     return GI_HIT_RIGHT;
            if ( (*g)[grow][gcol] != 0 ) return GI_COLLISION;

            (*g)[grow][gcol] = t->color;
        }
    }

    return GI_OK;
}

void grid_remove_filled_rows(grid_t *g)
{
    grid_t grid;
    int grid_row;

    memset(grid, 0, sizeof(grid_t));
    grid_row = 0;

    for (int row = 0; row < GRID_ROWS; ++row)
    {
        bool full = true;

        for (int col = 0; col < GRID_COLS; ++col)
        {
            if ( (*g)[row][col] == 0 )
            {
                full = false;
                break;
            }
        }

        if ( !full )
        {
            memcpy(&grid[grid_row], &(*g)[row], GRID_COLS * sizeof(uint32_t));
            grid_row++;
        }
    }

    grid_cpy(g, &grid);
}


void date_set_from_now(date_t *date, struct timeval time)
{
    date_t now;
    gettimeofday(&now, NULL);
    timeradd(&now, &time, date);
}

bool date_isexpired(date_t *date)
{
    date_t now;
    gettimeofday(&now, NULL);
    return ( timercmp(date, &now, <) != 0 );
}

void date_delay(date_t *date, struct timeval time)
{
    date_t delayed = *date;
    timeradd(&delayed, &time, date);
}


void cb_display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glTranslatef(-1.0, -1.0, 0.0);
    glScalef(2.0 / GRID_COLS, 2.0 / GRID_ROWS, 0.0);

    for (int row = 0; row < GRID_ROWS; ++row)
    {
        for (int col = 0; col < GRID_COLS; ++col)
        {
            GLubyte r = COLOR_R(g_grid[row][col]);
            GLubyte g = COLOR_G(g_grid[row][col]);
            GLubyte b = COLOR_B(g_grid[row][col]);
            GLubyte a = COLOR_A(g_grid[row][col]);

            glColor4ub(r, g, b, a);
            glBegin(GL_POLYGON);
                glVertex2f(col + 0, row + 0);
                glVertex2f(col + 1, row + 0);
                glVertex2f(col + 1, row + 1);
                glVertex2f(col + 0, row + 1);
            glEnd();
        }
    }

    glPopMatrix();

    glutSwapBuffers();
}

void cb_keyboard(unsigned char key, int _, int __)
{
    tetro_t tetro = g_tetro;
    grid_t  grid;
    int gi;

retry_insertion:

    switch (key)
    {
        case 'z': tetro_transform(&tetro,  0,  1,  0); break;
        case 'q': tetro_transform(&tetro, -1,  0,  0); break;
        case 's': tetro_transform(&tetro,  0, -1,  0); break;
        case 'd': tetro_transform(&tetro,  1,  0,  0); break;
        case 'r': tetro_transform(&tetro,  0,  0, -1); break;
        default:
            break;
    }

    grid_cpy(&grid, &g_grid_committed);
    gi = grid_insert_tetro(&grid, &tetro);

    switch (gi)
    {
        case GI_HIT_LEFT : key = 'd'; goto retry_insertion;
        case GI_HIT_RIGHT: key = 'q'; goto retry_insertion;
        case GI_HIT_TOP  : key = 's'; goto retry_insertion;
        case GI_HIT_BOT  : key = 'z'; goto retry_insertion;

        case GI_OK:
            g_tetro = tetro;
            grid_cpy(&g_grid, &grid);
            glutPostRedisplay();
            break;

        default:
            break;
    }
}

void cb_idle(void)
{
    tetro_t tetro;
    grid_t  grid;
    int gi;

    if ( !date_isexpired(&g_date_fall) )
        return;

    tetro = g_tetro;
    grid_cpy(&grid, &g_grid_committed);

    tetro_transform(&tetro, 0, -1, 0);
    gi = grid_insert_tetro(&grid, &tetro);

    if ( gi == GI_OK )
    {
        g_tetro = tetro;
        grid_cpy(&g_grid, &grid);
        glutPostRedisplay();
    }
    else if ( gi == GI_COLLISION || gi == GI_HIT_BOT )
    {
        tetro_init(&g_tetro);
        grid_remove_filled_rows(&g_grid);
        grid_cpy(&g_grid_committed, &g_grid);
    }
    else
    {
        assert( !"Invalid GI value when going down" );
    }

    date_set_from_now(&g_date_fall, (struct timeval){ 1, 0 });
}


int main(int argc, char *argv[])
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(300, 750);
    glutCreateWindow("737|215");

    glutDisplayFunc(cb_display);
    glutKeyboardFunc(cb_keyboard);
    glutIdleFunc(cb_idle);

    srand(time(NULL));
    tetro_init(&g_tetro);
    date_set_from_now(&g_date_fall, (struct timeval){ 0, 0 });

    glutMainLoop();

    return 0;
}
