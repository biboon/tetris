# Tetris

Just a toy project to experiment with openGL.

## TODO

- Make animation more fluid
- Change colors
- Give a more pleasant look to squares
- Increase speed as time passes
- Block use of 'z' key from user
